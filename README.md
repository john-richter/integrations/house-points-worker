# House Points Worker

A service which handles all work required to run the House Points competition.

## Purpose

Handles all long running work, e.g. submitting points to the backend, involved with the House Points game. Receives
work through the [House Points Event Bus](https://gitlab.com/johnrichter/house-points/-/tree/master/event/bus).

# Env

This app relies on many different configuration variables. In production some vars are expected to be encrypted using
AWS KMS.

## Config

### Datadog

The following config options are set for enhanced metrics and tracing of the Lambda function with Datadog.

- `DD_ENHANCED_METRICS` set to `true`
- `DD_ENV` set to `dev` or `prod`
- `DD_FLUSH_TO_LOG` set to `true`
- `DD_KMS_API_KEY` set to a Datadog API Key that is KMS Encrypted
- `DD_LOG_LEVEL` set to error to limit log volumes
- `DD_MERGE_XRAY_TRACES` set to `true`
- `DD_SERVICE` set to the name of the service in Datadog, i.e. `gtm-house-points-worker`
- `DD_TRACE_ENABLED` set to `true`
- `DD_VERSION` set to the version of the deployment, e.g. `v1.0.4`

### App

These options control app specific behavior

- `HP_EVENT_BUS_MAX_RETRY_ATTEMPTS` a number > 1 that defines how many times a House Points even will be retried
- `HP_SQS_MESSAGE_GROUP_ID` a unique identifer which groups House Points within an SQS queue
- `HP_SQS_QUEUE_URL` the URL of the SQS Queue
- `HP_GTM_WEB_STORAGE_SERVICE` the service name of the GTM Website in Datadog
- `HP_HOUSE_POINTS_LEADERBOARD_URL` the URL for the leaderboard where users can view current results
- `HP_HOUSE_POINTS_MANUAL_SUBMISSION_URL` the URL where users can manually submit points outside of Slack
- `HP_HOUSE_POINTS_SUBMISSION_URL` the URL where House Points will be programatically submitted
- `HP_SLACK_CONFIG_DATA` the Slack App secrets as JSON, KMS encrypted
  ```json
  {
    "slack_client_id": "...",
    "slack_client_secret": "...",
    "slack_signing_secret": "...",
    "slack_verification_token": "...",
    "slack_bot_token": ".."
  }
  ```

# Development

Given the polyrepo setup of the House Points components, you may need to specify local locations for one or more
dependencies. Use the `go.mod.local` file as a guide to build and test this repo locally

> May require the new Docker BuildKit. `export DOCKER_BUILDKIT=1` to enable.
