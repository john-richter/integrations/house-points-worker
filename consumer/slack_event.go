package consumer

import (
	"context"

	"github.com/rs/zerolog/log"
	"github.com/slack-go/slack"
	"github.com/slack-go/slack/slackevents"
	hp "gitlab.com/johnrichter/house-points"
	hpevent "gitlab.com/johnrichter/house-points/event"
	"gitlab.com/johnrichter/logging-go"
	slackapp "gitlab.com/johnrichter/slack-app"
)

func (c *EventConsumer) processHousePointsSlackEvent(ctx context.Context, hpe *hpevent.HousePointsEvent) error {
	switch hpe.Slack.Type {
	case hpevent.SlackEventTypeEventsAPI:
		return c.processSlackEventsAPIEvent(ctx, hpe)
	case hpevent.SlackEventTypeInteraction:
		return c.processSlackInteraction(ctx, hpe)
	case hpevent.SlackEventTypeSlashCommand:
	}
	log.Debug().
		Str(hp.LogHousePointsSlackEventType, hpe.Slack.EventsAPIEvent.Event.Type).
		Msg("Unknown Slack Event type")
	return nil
}

func (c *EventConsumer) processSlackEventsAPIEvent(ctx context.Context, hpe *hpevent.HousePointsEvent) error {
	switch e := hpe.Slack.EventsAPIEvent.Event.InnerEvent.Data.(type) {
	case *slackevents.LinkSharedEvent:
		hplsEvent := hpevent.GetLinkSharedEventFromSlackEvent(&hpe.Slack.EventsAPIEvent.Callback, e)
		return c.storeLinkShared(ctx, hpe, hplsEvent)
	case *slackevents.ReactionAddedEvent:
		hprEvent := hpevent.GetReactionEventFromSlackAddEvent(&hpe.Slack.EventsAPIEvent.Callback, e)
		return c.storeReaction(ctx, hpe, hprEvent)
	case *slackevents.ReactionRemovedEvent:
		hprEvent := hpevent.GetReactionEventFromSlackRemoveEvent(&hpe.Slack.EventsAPIEvent.Callback, e)
		return c.storeReaction(ctx, hpe, hprEvent)
	default:
		log.Info().
			Str(logging.SlackEventType, hpe.Slack.EventsAPIEvent.Event.Type).
			Msg("Unknown Slack Events API Event type")
	}
	return nil
}

func (c *EventConsumer) processSlackInteraction(ctx context.Context, hpe *hpevent.HousePointsEvent) error {
	switch hpe.Slack.Interaction.CallbackID {
	case hp.HousePointsInteractionIDGlobal, hp.HousePointsInteractionIDMessage:
		// HousePoints does not implement DialogSuggestion, InteractionMessage, or BlockActions
		switch hpe.Slack.Interaction.Type {
		case slack.InteractionTypeDialogSubmission:
			ds, err := hpevent.NewHousePointsDialogSubmission(hpe.Slack.Interaction.Submission)
			if err != nil {
				return err
			}
			ape := hpevent.GetAwardPointsEventFromSlackInteraction(&hpe.Slack.Interaction, ds)
			c.storeAwardPoints(ctx, hpe, ape)
		case slackapp.SlackInteractionTypeGlobalShortcut,
			slack.InteractionTypeMessageAction,
			slack.InteractionTypeDialogCancellation:
			return nil // No-op for House Points event processing at the moment
		default:
			log.Debug().
				Str(logging.SlackInteractionID, hpe.Slack.Interaction.CallbackID).
				Str(logging.SlackInteractionType, string(hpe.Slack.Interaction.Type)).
				Msg("Unknown slack interaction type")
		}
	}
	log.Debug().Str(logging.SlackInteractionID, hpe.Slack.Interaction.CallbackID).Msg("Unknown slack interaction ID")
	return nil
}
