package consumer

import (
	"context"

	hpevent "gitlab.com/johnrichter/house-points/event"
)

func (c *EventConsumer) processHousePointsEvent(ctx context.Context, hpe *hpevent.HousePointsEvent) error {
	switch e := hpe.Event.(type) {
	case *hpevent.AwardPointsEvent:
		return c.storeAwardPoints(ctx, hpe, e)
	case *hpevent.LinkSharedEvent:
		return c.storeLinkShared(ctx, hpe, e)
	case *hpevent.ReactionEvent:
		return c.storeReaction(ctx, hpe, e)
	}
	return nil
}
