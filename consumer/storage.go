package consumer

import (
	"context"

	"github.com/rs/zerolog/log"
	hp "gitlab.com/johnrichter/house-points"
	hpevent "gitlab.com/johnrichter/house-points/event"
)

func (c *EventConsumer) storeAwardPoints(
	ctx context.Context,
	hpe *hpevent.HousePointsEvent,
	ape *hpevent.AwardPointsEvent,
) error {
	errors := []error{}
	for _, s := range c.stores {
		if err := s.AwardPoints(ctx, hpe, ape); err != nil {
			errors = append(errors, err)
			log.Warn().Err(err).Str(hp.LogStorageID, s.PluginID()).Msg("Unable to save house points award")
		}
	}
	return c.reportErrorForStoreFailures(errors)
}

func (c *EventConsumer) storeLinkShared(
	ctx context.Context,
	hpe *hpevent.HousePointsEvent,
	lse *hpevent.LinkSharedEvent,
) error {
	errors := []error{}
	for _, s := range c.stores {
		if err := s.LinkShared(ctx, hpe, lse); err != nil {
			errors = append(errors, err)
			log.Warn().
				Err(err).
				Str(hp.LogStorageID, s.PluginID()).
				Msg("Unable to save house points link share")
		}
	}
	return c.reportErrorForStoreFailures(errors)
}

func (c *EventConsumer) storeReaction(
	ctx context.Context,
	hpe *hpevent.HousePointsEvent,
	re *hpevent.ReactionEvent,
) error {
	errors := []error{}
	for _, s := range c.stores {
		if err := s.Reaction(ctx, hpe, re); err != nil {
			errors = append(errors, err)
			log.Warn().Err(err).Str(hp.LogStorageID, s.PluginID()).Msg("Unable to save house points reaction")
		}
	}
	return c.reportErrorForStoreFailures(errors)
}

func (c *EventConsumer) reportErrorForStoreFailures(errors []error) error {
	errorCount := len(errors)
	if errorCount > 0 {
		if errorCount < c.storeCount {
			//
			// TODO: Return error to reprocess? Is partial data loss better than duplicate data?
			//
			log.Warn().Msgf("Unable to save house points on %d / %d stores. Data loss possible")
		} else {
			log.Error().Msgf("Unable to save house points on all data stores")
			return hpevent.ErrStorageFailure
		}
	}
	return nil
}
