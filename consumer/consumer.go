package consumer

import (
	"context"

	"github.com/rs/zerolog/log"
	hp "gitlab.com/johnrichter/house-points"
	hpevent "gitlab.com/johnrichter/house-points/event"
)

type EventConsumer struct {
	stores     []hp.HousePointsStoragePlugin
	storeCount int
}

func NewHousePointsEventConsumer(s []hp.HousePointsStoragePlugin) *EventConsumer {
	return &EventConsumer{stores: s, storeCount: len(s)}
}

func (c *EventConsumer) ProcessEvent(ctx context.Context, hpe *hpevent.HousePointsEvent) error {
	switch hpe.Origin {
	case hpevent.EventOriginHousePoints:
		return c.processHousePointsEvent(ctx, hpe)
	case hpevent.EventOriginSlack:
		return c.processHousePointsSlackEvent(ctx, hpe)
	}
	log.Debug().Str("house_points.event.type", hpe.Type).Msg("Unknown House Points event type")
	return hpevent.ErrUnknownHousePointsEventType
}
