FROM golang:1 as gtm-build-lambda
WORKDIR /app
COPY . .
RUN --mount=type=cache,target=/root/.cache/go-build \
    --mount=type=cache,target=/go/pkg/mod \
    go mod tidy && \
    go build -o ./build/lambda/house_points_worker ./cmd/lambda

FROM public.ecr.aws/lambda/go:1
COPY --from=gtm-build-lambda /app/build/lambda/house_points_worker ${LAMBDA_TASK_ROOT}
CMD ["house_points_worker"]
