FROM golang:1 as gtm-build-lambda
COPY ./gtm/house-points-storage-plugin-gtm /apps/gtm/house-points-storage-plugin-gtm/
COPY ./projects/house-points-worker /apps/projects/house-points-worker/
COPY ./projects/house-points /apps/projects/house-points/
COPY ./projects/logging-go /apps/projects/logging-go/
COPY ./projects/tracing-go /apps/projects/tracing-go/
COPY ./projects/slack-app /apps/projects/slack-app/
WORKDIR /apps/projects/house-points-worker
RUN --mount=type=cache,target=/root/.cache/go-build \
    --mount=type=cache,target=/go/pkg/mod \
    go mod tidy && \
    go build -o ./build/lambda/house_points_worker ./cmd/lambda

FROM public.ecr.aws/lambda/go:1
COPY --from=gtm-build-lambda /apps/projects/house-points-worker/build/lambda/house_points_worker ${LAMBDA_TASK_ROOT}
CMD ["house_points_worker"]
